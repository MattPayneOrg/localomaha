'use strict';
require('angular');
require('angular-route');
require('angular-filter');
require('angular-resource');
require('angular-sanitize');
var app = angular.module('localOmaha', [
    'ngResource',
    'ngRoute',
    'angular.filter',
    'ngSanitize'
  ]);
//app.constant('VERSION', require('../../package.json').version);
require('./localOmaha');
require('./latlon/index');
app.config([
  '$routeProvider',
  function ($routeProvider) {
    $routeProvider.when('/local/:businessId', {
      templateUrl: 'js/localOmaha/local.omaha.compass.html',
      controller: 'localOmahaCompass',
      resolve: {
        list: function ($route, localOmahaResource) {
          return localOmahaResource.getAllLocalOmahaBusinesses().$promise;
        }
      }
    }).otherwise({
      redirectTo: '/',
      templateUrl: 'js/localOmaha/local.omaha.business.list.html',
      controller: 'localOmahaBusinessList'
    });
  }
]);