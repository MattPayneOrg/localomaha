'use strict';
module.exports = function ($log) {
  function watchUserLocation(successCallback, errorCallback) {
    var id;
    if ('geolocation' in navigator) {
      id = navigator.geolocation.watchPosition(successCallback, errorCallback);
    } else {
      $log.info('browser does not support geolocation API.');
    }
    return id;
  }
  function clearWatch(geolocationWatchId, orientationCallback) {
    navigator.geolocation.clearWatch(geolocationWatchId);
    window.removeEventListener('deviceorientation', orientationCallback);
  }
  function watchDeviceOrientation(callback) {
    if (window.DeviceOrientationEvent) {
      window.addEventListener('deviceorientation', callback);
    } else {
      $log.info('device orientation not supported');
    }
  }
  return {
    watchUserLocation: watchUserLocation,
    watchDeviceOrientation: watchDeviceOrientation,
    clearWatch: clearWatch
  };
};