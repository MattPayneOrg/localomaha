'use strict';
module.exports = function ($resource) {
  var data = $resource('data/simple75withLatLong_formatted.json');
  function getAllLocalOmahaBusinesses() {
    return data.query();
  }
  return { getAllLocalOmahaBusinesses: getAllLocalOmahaBusinesses };
};