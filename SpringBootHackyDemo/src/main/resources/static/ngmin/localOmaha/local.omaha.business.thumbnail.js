'use strict';
var latlon = require('../latlon');
module.exports = function () {
  return {
    restrict: 'E',
    templateUrl: 'js/localOmaha/local.omaha.business.thumbnail.html',
    scope: {
      business: '=',
      userLocation: '=',
      userDeviceOrientation: '=',
      showCompass: '&'
    },
    controller: function ($scope, $sce) {
      var businessLocation = {
          lat: $scope.business.lat,
          lon: $scope.business.lng
        };
      $scope.addressFormatted = $sce.trustAsHtml($scope.business.address.replace(',', '<br>'));
      $scope.getDistance = function () {
        if ($scope.userLocation) {
          $scope.units = 'miles';
          $scope.business.distance = latlon.getDistanceFromCoords($scope.userLocation, businessLocation);
          $scope.business.showDistance = !isNaN($scope.business.distance);
          return $scope.business.distance;
        }
      };
      $scope.getDirection = function () {
        if ($scope.userLocation) {
          $scope.business.direction = latlon.getBearingFromCoords($scope.userLocation, businessLocation) - $scope.userDeviceOrientation;
          $scope.business.showDirection = !isNaN($scope.business.direction);
          return 'rotate(' + $scope.business.direction + 'deg)';
        }
      };
    }
  };
};