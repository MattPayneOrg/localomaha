'use strict';
module.exports = function ($log, $http, $q) {
  var LOCALOMA_URL = '/localOmahaBusinesses';
  var CATEGORY_URL = '/categories';
  function getLocalOmahaBusinessesByCategory(category) {
    return $http.get(LOCALOMA_URL + '?category=' + category).then(function (response) {
      return response.data;
    }, function (error) {
      $log.error('Failed to get by category ' + category);
      return $q.reject(error);
    });
  }
  function getLocalOmahaCategories() {
    return $http.get(CATEGORY_URL).then(function (response) {
      return response.data;
    }, function (error) {
      $log.error('Failed to get categories');
      return $q.reject(error);
    });
  }
  return {
    getLocalOmahaBusinessesByCategory: getLocalOmahaBusinessesByCategory,
    getLocalOmahaCategories: getLocalOmahaCategories
  };
};