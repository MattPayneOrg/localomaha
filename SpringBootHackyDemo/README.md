This started with https://github.com/payne/mhack2h2 which started with https://github.com/payne/mhack1 which is a combination of:

1. https://github.com/spring-projects/spring-boot/tree/master/spring-boot-samples/spring-boot-sample-jetty-ssl
1. https://spring.io/guides/gs/rest-service/
1. https://thoughtfulsoftware.wordpress.com/2015/01/25/setting-up-https-for-spring-boot-2/

Do this:

~~~~~~~~~~~~~~~~~~
gradle build
java -jar build/libs/localomaha-0.1.0.jar
~~~~~~~~~~~~~~~~~~

Then visit this URL

https://localhost:8443


Background
-----

This is a simple spring boot container that holds the app from LocalOmahaWebapp (the app sub-folder)

There's an h2 database inside here.   See src/main/resources/(schema.sql|data.sql) for the schema and starting data.   In the context of 
this project, that schema and data do not mean much (they are left over from mhack2h2).  They are left in here to remind folks
how easy it is to have an h2 database.   Connecting to a long running server instead of an embedded database is also possible.

https://spring.io/guides for more info.


