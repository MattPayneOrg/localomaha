#!/bin/sh
# This file initializes the VM; it is run by the Vagrant setup in vagrant/

# Install any additional packages
sudo pkg install -y vim-lite

# Install PostgreSQL + PostGIS
/usr/local/bin/rsetup postgresql

# Tell gem to not install documentation
sudo echo "gem: --no-ri --no-rdoc" >> ~vagrant/.gemrc
sudo chown vagrant:vagrant ~vagrant/.gemrc

# Set the vagrant user to install RVM and Ruby on first login
echo "if [ ! -f ~/.installedrvm ]; then" >> ~vagrant/.profile
echo "  echo 'Installing RVM and Ruby 2.1.5'" >> ~vagrant/.profile
echo "  /usr/local/bin/rsetup rvm >/dev/null && source ~/.rvm/scripts/rvm && rvm install --default 2.1.5 >/dev/null && gem install bundler >/dev/null" >> ~vagrant/.profile
echo "  echo 'Installing gems for Local Omaha REST Service'" >> ~vagrant/.profile
echo "  cd /src && bundle install >/dev/null" >> ~vagrant/.profile
echo "  echo 'Gems installed'" >> ~vagrant/.profile
echo "  echo 'Installing PostgreSQL gem (pg)'" >> ~vagrant/.profile
echo "  gem install pg" >> ~vagrant/.profile
echo "  echo 'pg gem installed'" >> ~vagrant/.profile
echo "  echo 'Adding PostGIS POINTs to the database'" >> ~vagrant/.profile
echo "  cd /src/setup && ruby ./gis.rb" >> ~vagrant/.profile
echo "  echo 'PostGIS POINTs added'" >> ~vagrant/.profile
echo "  touch ~/.installedrvm" >> ~vagrant/.profile
echo "fi" >> ~vagrant/.profile
echo "source ~/.rvm/scripts/rvm" >> ~vagrant/.profile

# Set up the database
sudo bash -c "echo 'local  all         all      trust'              > /usr/local/pgsql/data/pg_hba.conf"
sudo bash -c "echo 'host   localomaha  lo_user  127.0.0.1/32  md5' >> /usr/local/pgsql/data/pg_hba.conf"
sudo bash -c "echo 'host   localomaha  lo_user  ::1/128       md5' >> /usr/local/pgsql/data/pg_hba.conf"
sudo /usr/local/etc/rc.d/postgresql restart >/dev/null
sudo -u pgsql createdb -T postgis_template localomaha
sudo -u pgsql psql -f /src/setup/localomaha.sql -d localomaha

# Copy the user's config (if needed)
if [ ! -f /src/config/config.yml ]; then
	if [ -f /src/config/config.yml.example ]; then
		echo "No configuration file found. Copying sample config..."
		cp /src/config/config.yml.example /src/config/config.yml
	fi
fi