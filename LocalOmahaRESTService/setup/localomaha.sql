-- {
--     "address": "6401 Dodge Street, Omaha, NE 68132, USA",
--     "category": "Coder Dojo",
--     "lat": 41.257888,
--     "lng": -96.010793,
--     "name": "Local Omaha Coder Dojo, Feb 7 12-5pm",
--     "number": 0
-- },

create user lo_user with password 'p4ssw0rd';
grant all privileges on database localomaha to lo_user;
	
begin;

create table "businesses" (
	"id" int4 not null,
	"category" varchar(500) not null,
	"address" varchar(500) not null,
	"lat" float4 not null,
	"lng" float4 not null,
  "name" varchar(500) not null,
  "facebook" varchar(500),
  "type" varchar(500) not null,
  "phone" varchar(500),
  "url" varchar(500),
  "store_url" varchar(500)
)
with (oids=false);
alter table "businesses" owner to lo_user;

grant truncate, insert, select, delete, trigger, references, update on table spatial_ref_sys to lo_user;
grant truncate, insert, select, delete, trigger, references, update on table geometry_columns to lo_user;
select AddGeometryColumn('businesses', 'coord', 4326, 'POINT', 2);

commit;