require 'sinatra/base'
require './helpers/init'

# /api/samples/:route
class SampleApi < Sinatra::Base
  get '/hello/:name' do
    ResponseHelper.format_response("Hello, #{params[:name]}!", request.accept)
  end
end