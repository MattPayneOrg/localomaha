class Business
  include DataMapper::Resource

  property :id,        Serial
  property :address,   Text
  property :lat,       Float, :index => :latlng
  property :lng,       Float, :index => :latlng
  property :name,      Text
  property :category,  Text
  property :facebook,  Text
  property :type,      Text
  property :phone,     Text
  property :url,       Text
  property :store_url, Text
end