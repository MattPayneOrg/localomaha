package org.localomaha;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.junit.Test;
import org.w3c.tidy.Tidy;

public class HelloJTidy {

	@Test
	public void test() throws IOException {
		Tidy tidy = new Tidy();
		tidy.setXHTML(true);
		File htmlFile = new File("src/main/resources/local5.html");
		assertTrue(htmlFile.exists());
		org.w3c.dom.Document w3cDom = tidy.parseDOM(new FileInputStream(
				htmlFile), null);
		assertNotNull(w3cDom);
		Document dom4jDoc = Utils.buildDocment(w3cDom);

		// saveToFile("dump.xml", dom4jDoc);

		// we want to find
		// <div class="adress_cell">
		// 423 S 13th St, Omaha, 68102
		// </div>
		// for some help with the xpath
		// http://stackoverflow.com/questions/18570622/selenium-and-xpath-finding-a-div-with-a-class-id-and-verifying-text-inside
		String xpath = "//div[contains(@class,'adress_cell')]";
		// xpath = "//div[@class='address_cell']";
		List<Element> lst = dom4jDoc.selectNodes(xpath);
		System.out.println("lst=" + lst);
		assertNotNull(lst);
		int n = 1;
		List<LocalBusiness> lstLocalBusiness = new ArrayList<LocalBusiness>();
		for (Element addr : lst) {
			// System.out.format("%d addr=%s\n", n++, addr.getText());
			LocalBusiness localBusiness = extractLocalBusiness(addr);
			extractCategory(localBusiness, addr);
			extractUrl(localBusiness, addr);
			System.out.format("%d localBusiness=%s\n", n++, localBusiness);
			lstLocalBusiness.add(localBusiness);
		}
		// Thanks, Mkyong!
		// http://www.mkyong.com/java/how-to-convert-java-object-to-from-json-jackson/
		// TODO: pretty print
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(
				new File("src/main/resources/first75AndCategory.json"),
				lstLocalBusiness);
	}

	private void extractUrl(LocalBusiness localBusiness, Element addr) {
		Element parent = addr.getParent();
		if (parent.elements().size()>=4) {
			Element four = (Element) parent.elements().get(4);
		four = (Element) four.elements().get(0);
			String urlText = four.getText();
			System.out.println("urlText=" + urlText);
			System.out.println("Well?");
		}
	}

	private void extractCategory(LocalBusiness localBusiness, Element addr) {
		Element greatGreatGrandParent = addr.getParent().getParent()
				.getParent().getParent().getParent().getParent().getParent();
		Element kid1 = (Element) greatGreatGrandParent.elements().get(0);
		String text = kid1.getText();
		System.out.println(text);
		localBusiness.setCategory(text);
		System.out.println("Well?");
	}

	private LocalBusiness extractLocalBusiness(Element addr) {
		LocalBusiness retval = new LocalBusiness();
		retval.setAddress(addr.getText());
		// we know this from looking at the XHTML/HTML source.
		// Go one two far and every company is The Imaginarium
		Element greatGrandParent = addr.getParent().getParent().getParent();
		// try 1:
		// List<Element> lst =
		// grandparent.selectNodes("//div[contains(@class,'legal_name')]");
		// retval.setName(lst.get(0).getText());
		// try 2:
		// retval.setName(grandparent.getText());

		// try 3:
		// Note, we need the // here to search through the h5 that is under the
		// greatGrandParent element/node
		// Maybe it's / and not //
		// Maybe it's just no slash?
		// Maybe I need to spell it out to h5/span?
		// Oh, it's h5/a/span?
		// List<Element> lst =
		// greatGrandParent.selectNodes("h5/a/span[contains(@class,'legal_name')]");
		// String name = lst.size() > 0 ? lst.get(0).getText() : "WRONG!";
		// retval.setName(name);

		// try 4: Let's just walk the dom...
		List<Element> kids = greatGrandParent.elements();
		Element h5 = kids.get(0);
		// System.out.println("h5="+h5.getText());
		List<Element> aKids = h5.elements();
		Element a0 = aKids.get(0);
		// System.out.println("a0="+a0.getText());
		List<Element> spanKids = a0.elements();
		Element span = spanKids.get(0);
		// System.out.println("span="+span.getText());
		retval.setName(span.getText());
		return retval;
	}

	private String getAttribute(Element element, String name) {
		Attribute attribute = element.attribute(name);
		String value = attribute.getValue();
		return value;
	}

	private void saveToFile(String fileName, Document dom4jDoc)
			throws IOException {
		// http://dom4j.sourceforge.net/dom4j-1.6.1/guide.html
		// OutputFormat format = OutputFormat.createPrettyPrint();
		// XMLWriter writer = new XMLWriter(new FileWriter(fileName), format);
		XMLWriter writer = new XMLWriter(new FileWriter(fileName));
		writer.write(dom4jDoc);
		writer.close();
	}

}
