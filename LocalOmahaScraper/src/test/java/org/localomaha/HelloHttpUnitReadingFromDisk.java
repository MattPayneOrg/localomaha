package org.localomaha;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;

public class HelloHttpUnitReadingFromDisk {

	@Test
	public void test() throws IOException, SAXException {
		WebConversation wc = new WebConversation();
	    WebRequest     req = new GetMethodWebRequest( "file://src/main/resources/local2.html" );
	 
	    WebResponse   resp = wc.getResponse( req );
	    System.out.println(resp);
	}

}
