require_relative 'store' # Many thanks Carl Z!
require 'geocoder'

input = File.open("stores_formatted_try2.json", "r")
lstStores = JSON.parse(input.read)
input.close
# https is required for the free google service
Geocoder.configure(:lookup => :google, :api_key=>ENV['GEOCODE_API_KEY'],:use_https => true)

latLngStores=[]
lstStores.each do |store|
  puts store
end
lstStores.each do |store|
  puts "#{store}"
  addr=store["address"]
  if (addr.size > 0)
    lat,lng=Geocoder.coordinates(addr)
    store["lat"]=lat
    store["lng"]=lng
    latLngStores << store
  else
    store["lat"]=0
    store["lng"]=0
  end
end

output = File.open("lat_lng.json", "w")
output <<  latLngStores.to_json
output.close

