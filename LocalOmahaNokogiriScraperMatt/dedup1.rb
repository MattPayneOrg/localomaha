require 'jsonable'
input = File.open("lat_lng_formatted.json", "r")
lstStores = JSON.parse(input.read)
input.close

puts "There are #{lstStores.length} entries in lstStores."
storeMap=Hash.new
lstStores.each do |store|
  url= store["url"]
  storeMap[url]=store
end
puts "There are #{storeMap.length} entries in storeMap."
lstStores=storeMap.values
output = File.open("lat_lng_no_dups.json", "w")
output <<  lstStores.to_json
output.close
