<!-- slide 2a -->
# [Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) with Art Doler

<!-- Speaker Notes
* define the flex container
* allow wrapping for _really_ narrow viewports
* justify the content
* and align to the baseline
Easy-peasy!
 -->
	display: flex;
	flex-wrap: wrap;
	justify-content: flex-start;
	align-items: baseline;
	
![List page](./app_screen_shot.png "Screen Shot")
	
	
<HR>
<!-- slide 2b -->
## Git Patch Mode
	$ git add -p
	diff --git a/LocalOmahaWebapp/app/assets/styles.css b/LocalOmahaWebapp/app/assets/styles.css
	index a4fbbbb..88672d5 100644
	--- a/LocalOmahaWebapp/app/assets/styles.css
	+++ b/LocalOmahaWebapp/app/assets/styles.css
	@@ -1,18 +1,23 @@
	 #compass-business {
	     float: left;
	-    width: 80%;
	+    width: 75%;
	     padding: 0.5em;
	 }
	 
	 #close-compass {
	     float: right;
	-    width: 8%;
	+    width: 15%;
	     font-size: 1.5em;
	+    text-align:right;
	     cursor: pointer;
	 }
	}
	Stage this hunk [y,n,q,a,d,/,j,J,g,s,e,?]? y
