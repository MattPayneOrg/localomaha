<!-- slide 1a -->
# Where I Started
<!-- Speaker Notes
The web app started as an "angular-seed" project. It has support for including Angular components with bower/npm. We had a bunch of undesirable/unmaintainable script includes. 
-->
## ["angular-seed" project](https://github.com/angular/angular-seed)
* Bundle AngularJS components with bower/npm
* Undesirable/unmaintainable list of script includes

### index.html

		<script src="bower_components/angular/angular.js"></script>
		<script src="bower_components/angular-filter/dist/angular-filter.js"></script>
		<script src="bower_components/angular-resource/angular-resource.js"></script>
		<script src="bower_components/angular-route/angular-route.js"></script>
		<script src="app.js"></script>
		<script src="latlon/latlon.js"></script>
		<script src="localOmaha/local.omaha.js"></script>
		<script src="localOmaha/local.omaha.business.list.js"></script>
		<script src="localOmaha/local.omaha.business.thumbnail.js"></script>
		<script src="localOmaha/local.omaha.resource.js"></script>
		<script src="localOmaha/local.omaha.service.js"></script>

<HR>
<!-- slide 1b -->
<!-- Speaker Notes
Angular magic allows you to use dependency injection with AngularJS components, but without support for commonjs, I had to define global functions for non Angular functionality. I suppose I could have rewritten latlon.js as a factory, but what I really wanted was to "require" the library it came from.
-->
* Prototyping to get the job started
* AngularJS dependency injection
* Global functions for non Angular functionality
### latlon.js
		function getDistanceFromCoords (from, to, decimals) {
		    ...
		}

		function getBearingFromCoords(from, to){
		    ...
		}

<HR>
<!-- slide 1c -->
<!-- Speaker Notes
  Browsers don't have the require method defined, but Node.js does. I was familiar with Browserify from the work I do at Aviture, and I had read that using RequireJS with an Angular project can be painful. So I chose Browserify, and I figured Nick would be able to help me with it because he's pretty much a JavaScript rockstar. *BUT* neither Nick or I knew how to configure it. Enter the AngularJS + Browserify Project Template.
  With Browserify you can write code that uses require in the same way that you would use it in Node. Browserify bundles your requires into a single script file and source map. (And if you want to learn more about source maps, you should go to NebraskaJS in Omaha next month where Matt Steele, another Omaha JavaScriot rockstar, will tell you all about them.) You can use browserify to organize your code and use third-party libraries even if you don't use node itself in any other capacity except for bundling and installing packages with npm.
-->
## Browserify with Nick Nisi
* [AngularJS + Browserify Project Template](https://github.com/basti1302/angular-browserify)
 
### index.html 
    <script src="dist/app.js"></script></body>
### app.js
	require('angular');
	require('angular-route');
	require('angular-filter');
	require('angular-resource');
	require('angular-sanitize');
	require('./localOmaha');
	require('./latlon/index');

<HR>
<!-- slide 1d -->
<!--
The Angular components became JavaScript modules. We included them with require.
-->
## desirable === CommonJS require
* The Angular components became JavaScript modules.
* We included them with require.

### localOmaha/index.js
	var app = angular.module('localOmaha');

	app.controller('localOmahaBusinessList', require('./local.omaha.business.list'));
	app.controller('localOmahaCompass', require('./local.omaha.compass'));

	app.directive('localOmahaBusinessThumbnail', require('./local.omaha.business.thumbnail'));

	app.factory('localOmahaResource', require('./local.omaha.resource'));
	app.factory('localOmahaService', require('./local.omaha.service'));
	app.factory('localOmahaUserLocation', require('./local.omaha.user.location'));

<HR>
<!-- slide 1e -->	
### Directives are defined as modules
	module.exports = function() {
	    return {
	        restrict: 'E',
	        templateUrl: 'js/localOmaha/local.omaha.business.thumbnail.html',
	        scope: {
	            ...
	        },
	        controller: function($scope) {
	            ...
	        }
	    };
	};

<HR>
<!-- slide 1f -->
<!-- Speaker Notes
I had wanted to bundle geodesy in with the app. Nick and I looked through the library. And well, if you've ever been to a hackathon, you might know that it's great for brainstorming and getting some quick things done. But often what you end up with is something that works along with some great ideas to expand on later. So instead of using geodesy, we made latlon.js into a module instead. It just seemed easier because we weren't too familiar with the geodesy library, and we already had something that worked. Better, but not best.
-->	
## latlon module
### latlon/index.js
	exports.getDistanceFromCoords = function(from, to, decimals) {
		...
	};
	exports.getBearingFromCoords = function(from, to) {
		...
	};
### Using the module in an AngularJS controller
	var latlon = require('../latlon');
	...
	latlon.getDistanceFromCoords($scope.userLocation, businessLocation)
	latlon.getBearingFromCoords($scope.userLocation, businessLocation)
