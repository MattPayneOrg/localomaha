# README #

This is a scratch area for the LocalOmaha.org project.   

http://LocalOmaha.org has some more info.

There are multiple projects here. 


1. LocalOmahaGeocoder  -- Simple java project that uses google's geocoding service
1. LocalOmahaNokogiriScraperMatt -- Simple bunch of ruby code that uses nokogiri to scrape and google's geocoder
1. LocalOmahaRESTService -- Ross's backend
1. LocalOmahaWebapp -- The angularJS app
1. Proposals -- some pitches
1. Talks -- holds the revealJS based talk we gave 3/20/15
